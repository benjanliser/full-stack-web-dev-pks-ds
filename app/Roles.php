<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Roles extends Model
{
    protected $fillable = ['name'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    public static function boot () {

        parent::boot();

        static::creating (function ($model) {

            if (empty($model->{$model->getKeyName()})) {

                $model->{$model->getKeyName() } = Str::uuid();
            }
        });
}

public function user()
{
    return $this->hashMany('App\User');
}

}