<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Comment extends Model
{
    protected $fillable = ['content', 'post_id', 'user_id',];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    public static function boot()
    {

        parent::boot();

        static::creating(function ($model) {

            if (empty($model->{$model->getKeyName()})) {

                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->user_id = auth()->user()->id;
        });
    }

    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function user()
    {
        return $this->belongsTo('App\user');
    }
}
