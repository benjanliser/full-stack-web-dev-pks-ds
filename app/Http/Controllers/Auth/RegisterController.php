<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\User;
use App\OtpCode;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'name' => 'required',
            'email' => 'required|unique:users,email|email' ,
            'user_name' => 'required|unique:users,user_name'
        ]); 

        if($validator->fails()){

            return response()->json($validator->errors(), 400);
        }

        $users = User::create($allRequest);

        do {
            $random = mt_rand(100000 , 999999);
            $check = OtpCode::where('otp' , $random)->first();
        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5) ,
            'user_id' => $users->id
        ]);

        return response()->json([
            'succes' => true,
            'message' => 'Data User berhasil ditambahkan' ,
            'data' => [
                'user' => $users,
                'otp_code' => $otp_code


            ]

            ]);

        
         
    }
}
