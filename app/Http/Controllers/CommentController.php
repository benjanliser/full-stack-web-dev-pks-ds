<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStoredEvent;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use App\Mail\CommentAuthorMail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request->post_id;
        $comments = Comment::where('post_id', $post_id)->latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar Comment berhasil ditampilkan',
            'data'    => $comments
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'content' => 'required',
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {

            return response()->json($validator->errors(), 400);
        }



        //get data from table posts
        $comments = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id,

        ]);

        //memanggil event CommentStoredEvent
        event(new CommentStoredEvent($comments));

        // //ini dikirim kepada yg memiliki post
        // Mail::to($comments->post->user->email)->send(new PostAuthorMail($comments));

        // //ini dikirim kepada yg memiliki comment
        // Mail::to($comments->user->email)->send(new CommentAuthorMail($comments));


        if ($comments) {

            return response()->json([
                'success' => true,
                'message' => 'Data Comments berhasil dibuat',
                'data'    => $comments
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Comment gagal dibuat',

        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $comments = Comment::find($id);

        if ($comments) {
            return response()->json([
                'success' => true,
                'message' => 'Data berhasil ditampilkan',
                'data' => $comments
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data degan id : ' . $id . 'tidak ditemukan'

        ], 404);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'content' => 'required',

        ]);

        if ($validator->fails()) {

            return response()->json($validator->errors(), 400);
        }
        $comments = Comment::find($id);

        if ($comments) {

            $user = auth()->user();
            if ($comments->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',

                ], 403);
            }
            $comments->update([

                'content' => $request->content,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data berhasil di updat',
                'data' => $comments

            ]);
        }
        return response()->json([

            'success' => false,
            'message' => 'Data dengan id : '  . $id . 'tidak ditemukan'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)

    {

        $comments = Comment::find($id);

        if ($comments) {


            $user = auth()->user();
            if ($comments->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',

                ], 403);
            }
            $comments->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data Post berhasil dihapus',

            ], 200);
        }
        return response()->json([

            'success' => false,
            'message' => 'Data dengan id : '  . $id . 'tidak ditemukan',
        ], 404);
    }
}
