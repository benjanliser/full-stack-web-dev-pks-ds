<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::latest()->get();

        //make response JSON
        return response()->json([
             'success' => true,
             'message' => 'List Data Post',
             'data'    => $roles	
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest , [
            'name' => 'required',
        
        ]); 

        if($validator->fails()){

            return response()->json($validator->errors(), 400);
        }



        //get data from table posts
         $roles = Roles::create([
             'name' => $request->name,
             

         ]);

         if($roles){

            return response ()->json([
                'success' => true,
                'message' => 'Data Post berhasil dibuat',
                'data'    => $roles
             ],200);
            

         }

         return response ()->json([
            'success' => false,
            'message' => 'Data Post gagal dibuat',
        
         ],409);
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Roles::find($id);

        if ($roles) {
        
        return response ()->json([
            'success' => true,
            'message' => 'Data Role berhasil ditampilkan lagi',
            'data' => $roles
        
         ],200);     

        }
         
         
        return response()->json([
            'success' => false,
            'message' => 'Data degan id : ' . $id. 'tidak ditemukan'  

        ],404); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
            $validator = Validator::make($allRequest , [
                'name' => 'required',
                
            ]); 
    
            if($validator->fails()){
    
                return response()->json($validator->errors(), 400);
            }
            $roles = Roles::find($id);

            if ($roles)
            {
                $roles->update([

                    'name' => $request->name,
                    
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'Data berhasil di updata' ,
                    'data' => $roles

                ]);


            }
            return response()->json([

                'success' => false,
                'message' => 'Data dengan id : '  . $id.'tidak ditemukan'
            ],404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = Roles::find($id);

            if ($roles)
            {

                $roles->delete();
                    return response()->json([
                    'success' => true,
                    'message' => 'Data role berhasil di hapus' ,
                    'data' => $roles

                ]);


            }
            return response()->json([

                'success' => false,
                'message' => 'Data dengan id : '  . $id.'tidak ditemukan'
            ],404);
     
    }
}
