<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{

    public function index()
    {

        //get data from table posts
        $posts = Post::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data'    => $posts
        ]);
    }


    public function store(Request $request)
    {

        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {

            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();


        //get data from table posts
        $posts = Post::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => $user->id

        ]);

        if ($posts) {

            return response()->json([
                'success' => true,
                'message' => 'Data Post berhasil dibuat',
                'data'    => $posts
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data Post gagal dibuat',

        ], 409);
    }

    public function show($id)
    {
        $posts = Post::find($id);

        if ($posts) {


            return response()->json([
                'success' => true,
                'message' => 'Data Post berhasil ditampilkan lagi',
                'data' => $posts

            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => 'Data degan id : ' . $id . 'tidak ditemukan'

        ], 404);
    }

    public function update(Request $request, $id)
    {

        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {

            return response()->json($validator->errors(), 400);
        }
        $posts = Post::find($id);



        if ($posts) {

            $user = auth()->user();
            if ($posts->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',

                ], 403);
            }

            $posts->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);
        }
        return response()->json([

            'success' => false,
            'message' => 'Data dengan id : '  . $id . 'tidak ditemukan'
        ], 404);
    }

    public function destroy($id)
    {

        $posts = Post::find($id);

        if ($posts) {

            $user = auth()->user();
            if ($posts->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login',

                ], 403);
            }

            $posts->delete();
            return response()->json([
                'success' => true,
                'message' => 'Data Post berhasil di hapus',
                'data' => $posts

            ]);
        }
        return response()->json([

            'success' => false,
            'message' => 'Data dengan id : '  . $id . 'tidak ditemukan'
        ], 404);
    }
}
